var myApp = angular.module('myApp', []);

myApp.directive('inputDir', function ($rootScope) {
        return {
            restrict: 'E',
            template: `
     <div>
     <p>What is you favorite color?</p>
     <input type="text" ng-model="colorInserted"></input>
     <button type="submit" ng-click="onSubmit()">Submit</button>
     </div>`,
            scope: {},
            link: function (scope) {

                scope.onSubmit = function () {
                    $rootScope.$emit('colorChange', scope.colorInserted);
                    scope.colorInserted = "";
                }
            }
        }
    }
);

myApp.directive('bannerDir', function ($rootScope) {
        return {
            restrict: 'E',
            template: `<div>
     <p>The color of the day is {{color}}!</p>
     </div>`,
            scope: {},
            link: function (scope) {
                scope.color = "green";
                $rootScope.$on('colorChange', function (event, data) {
                    scope.color = data;
                });
            }
        }
    }
);



